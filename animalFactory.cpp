///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalFactory.cpp
/// @version 1.0
///
/// Generates random Animal classes
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <random>

#include "animalFactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;

namespace animalfarm {
	
random_device randomDevice;        // Seed with a real random value, if available
mt19937_64 RNG( randomDevice() );  // Define a modern Random Number Generator

	
Animal* AnimalFactory::getRandomAnimal() {
	uniform_int_distribution<> animalRNG(0, 5);
	int animalNumber = animalRNG( RNG );

	Animal* animal = 	NULL;
	switch( animalNumber ) {
		case 0: animal = new Cat( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() );
			     break;
		case 1: animal = new Dog( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() );
			     break;
		case 2: animal = new Nunu( Animal::getRandomBool(), RED, Animal::getRandomGender() );
			     break;
		case 3: animal = new Aku( Animal::getRandomWeight(16.0, 30.0), SILVER, Animal::getRandomGender() );
			     break;
		case 4: animal = new Palila( Animal::getRandomName(), YELLOW, Animal::getRandomGender() );
			     break;
		case 5: animal = new Nene( Animal::getRandomName(), BROWN, Animal::getRandomGender() );
			     break;
	}

	return animal;
}


} // namespace animalfarm

