///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file test.cpp
/// @version 2.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "animalFactory.hpp"
#include "animal.hpp"
#include "cat.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	cout << "I am Sam" << endl;
	
	for( auto i = 0 ; i < 10 ; i++ ) {
		//cout << Animal::genderName( Animal::getRandomGender() ) << endl;
		//cout << Animal::colorName( Animal::getRandomColor() ) << endl;
		//cout << boolalpha << Animal::getRandomBool() << endl;
		//cout << Animal::getRandomWeight( 4.0, 8.0 ) << endl;
		cout << Animal::getRandomName() << endl;
		//Animal* animal = AnimalFactory::getRandomAnimal();
		//cout << animal->speak() << endl; 
	}
	
	return 0;
}

