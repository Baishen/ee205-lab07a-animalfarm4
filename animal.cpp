///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <cstdlib>
#include <random>

#include "animal.hpp"

using namespace std;

namespace animalfarm {
	
static random_device          randomDevice;                    // Seed with a real random value, if available
static mt19937_64             RNG         ( randomDevice() );  // Define a modern Random Number Generator
static bernoulli_distribution boolRNG     ( 0.5 );             // Get a nice 0.5 Bernouli distribution


Animal::Animal() {
	cout << "." ;
}


Animal::~Animal() {
	cout << "x" ;
}


void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


const string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};


const Gender Animal::getRandomGender() {
	if( boolRNG( RNG ) ) 
		return MALE;
	else
		return FEMALE;
}


const string Animal::colorName (enum Color color) {
   switch (color) {
      case BLACK:  return string("Black"); break;
      case WHITE:  return string("White"); break;
      case RED:    return string("Red"); break;
      case BLUE:   return string("Blue"); break;
      case GREEN:  return string("Green"); break;
      case PINK:   return string("Pink"); break;
      case SILVER: return string("Silver"); break;
      case YELLOW: return string("Yellow"); break;
      case BROWN:  return string("Brown"); break;
   }

   return string("Unknown");
};

const enum Color Animal::getRandomColor() {
	uniform_int_distribution<> colorRNG(BLACK, BROWN);
	int r = colorRNG( RNG );

	switch( r ) {
		case 0: return BLACK;  break;
		case 1: return WHITE;  break;
		case 2: return RED;    break;
		case 3: return BLUE;   break;
		case 4: return GREEN;  break;
		case 5: return PINK;   break;
		case 6: return SILVER; break;
		case 7: return YELLOW; break;
		case 8: return BROWN;  break;
	}
	
	return BLACK;  // Should never get here
}


const bool Animal::getRandomBool() {
	return boolRNG( RNG );
}


const float Animal::getRandomWeight( const float from, const float to ) {
	uniform_real_distribution<> floatRNG(from, to);
	float f = floatRNG( RNG );
	
	return f;
}


const string Animal::getRandomName() {
	uniform_int_distribution<> lengthRNG(4, 9);

	static const char consonents[] = "bcdfghjklmnpqrstvwxyz";
	static const char vowels[] = "aeiouy";

	uniform_int_distribution<> uppercaseRNG('A', 'Z');
	uniform_int_distribution<> consonentRNG(0, sizeof( consonents ));
	uniform_int_distribution<> vowelRNG(0, sizeof( vowels ));

	string name = "";
	int len = lengthRNG( RNG );

	name += (char) uppercaseRNG( RNG );
	for ( int i = 1 ; i < len ; i++ ) {
		name += consonents[consonentRNG( RNG )];
		name += vowels[vowelRNG( RNG )];
	}

	return name;
}

} // namespace animalfarm

