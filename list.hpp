// TODO Insert the file comment block here
#pragma once

#include "node.hpp"

class SingleLinkedList {

protected:

   Node* head = nullptr;

public: 
   //returns true if list is empty, returns false if it's not
   const bool empty() const;
   void push_front( Node* newNode ); 

};

/*
Node* pop_front();

Node* get_first() const;

Node* get_next( const Node* currentNode ) const;

unsigned int size() const;
*/

