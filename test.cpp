#include <iostream>
#include <array>
#include <memory>
#include <list>
#include <cassert>

#include "animal.hpp"
#include "animalFactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "node.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;


int main(){

   cout << "Starting Animal Farm 4 Unit Test:" << endl;

   SingleLinkedList list;

   assert(list.empty());
   
   Node* newNode = new Node();

   list.push_front(newNode);

   assert(!list.empty());

}

